import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-my-acc',
  templateUrl: './my-acc.component.html',
  styleUrls: ['./my-acc.component.scss']
})
export class MyAccComponent implements OnInit {

  accounForm: FormGroup;
  alertHidden: boolean = true;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService
    ) {}

  ngOnInit() {
    this.accounForm = this.formBuilder.group({
      "userEmail": ["", [ Validators.required, Validators.email]],
      "userName": ["", Validators.required],
      "userSurname": ["", Validators.required],
      "userPhone": ["", [Validators.required, Validators.pattern("[0-9]{1,11}")]]
    });
  }

  submit() {
    this.userService.sendData(this.accounForm.value);
    this.accounForm.reset();
    this.alertHidden = false;
    setTimeout(() => {
      this.alertHidden = true;
    }, 2 * 1000)
  }

}