import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Subscription, Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AutoUnsubscribe } from "ngx-auto-unsubscribe";
import * as moment from 'moment';
import * as orderActions from '../../store/actions/orders'
import * as fromRoot from '../../store/reducers/index.reducers';
import { OrdersService } from 'src/app/services/orders.service';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@AutoUnsubscribe()  
@Component({
  selector: 'app-add-order',
  templateUrl: './add-order.component.html',
  styleUrls: ['./add-order.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddOrderComponent implements OnInit, OnDestroy {

  moment = moment(); 
  currentMonth = this.moment.format('MM[.]YYYY');
  orderForm: FormGroup;
  ordersForLastMonth: Number;
  sub1$: Subscription;
  sub2$: Subscription;
  newOrder: boolean = true;

  //form params
  date = this.moment.format('DD[.]MM[.]YYYY');
  idValue: string;
  type: string = 'Розница';
  customerEmail: string = '';
  customerName: string = '';
  customerSurname: string = '';
  customerPhone: string = '';
  positions: string[] = [];
  provider: string = 'Undead';
  completeDate: string = '';
  comment: string = '';

  url: string;

  private routeSubscription: Subscription;
  private querySubscription: Subscription;
  private urlSubscription: Subscription;

  constructor(
    private store: Store<fromRoot.State>,
    private activatedRoute: ActivatedRoute,
    private userService: UserService) {
  }

  ngOnInit() {
    this.userService.getData().subscribe(data => {
      if (data !== null) {
        this.customerEmail = data['userEmail'];
        this.customerName = data['userName'];
        this.customerPhone = data['userPhone'];
        this.customerSurname = data['userSurname'];
      }
    })
    this.urlSubscription = this.activatedRoute.url.subscribe(url => {
      if (url[0].path === 'addorder') {
        this.url = url[0].path;
        this.createForm();
        this.createId();
        this.defineFirstIdSymbol();
      } else if (url[0].path === 'editorder') {
        this.url = url[0].path;
        this.newOrder = false;
        this.routeSubscription = this.activatedRoute.params.subscribe(params => {
          this.idValue = params.id;
        });
        this.querySubscription = this.activatedRoute.queryParams.subscribe(query => { 
          this.customerEmail = query.customerEmail;
          this.customerName = query.customerName;
          this.customerPhone = query.customerPhone;
          this.customerSurname = query.customerSurname;
          this.date = query.dateCreateOrder;
          this.completeDate = query.dateFulfillment;
          this.positions = query.positions;
          this.provider = query.provider;
          this.type = query.type;
          this.comment = query.comment;
          this.createForm();
        });
      }
    });
    
    this.store.dispatch(new orderActions.GetCountOrdersMonth());
  }

  createId() {
    this.sub1$ = this.store.select(fromRoot.getOrdersForTheLastMongth)
      .subscribe(cur => {
      this.ordersForLastMonth = cur;
      this.idValue = this.generateId();
    });
  }

  defineFirstIdSymbol() {
    this.sub2$ = this.orderForm.controls.type.valueChanges.subscribe(type => {
      let arr = [this.idValue.charAt(0), this.idValue.slice(1)];
      arr[0] = type.charAt(0).toLowerCase();
      this.idValue = arr.join('');
    });
  }

  createForm() {
    this.orderForm = new FormGroup({
      "customerEmail": new FormControl(this.customerEmail, [Validators.required, Validators.email]),
      "customerName": new FormControl(this.customerName, Validators.required),
      "customerSurname": new FormControl(this.customerSurname, Validators.required),
      "customerPhone": new FormControl(this.customerPhone, [Validators.required, Validators.pattern("[0-9]{0,11}")]),
      "positions": new FormArray([
        new FormControl(this.positions[0], Validators.required)
      ]),
      "type": new FormControl(this.type, Validators.required),
      "provider": new FormControl(this.provider, Validators.required),
      "id": new FormControl(this.idValue),
      "completeDate": new FormControl(this.completeDate, Validators.required),
      "comment": new FormControl(this.comment)
    });
    if(this.positions.length > 1) {
      for(let i = 1; i < this.positions.length; i++) {
        (<FormArray>this.orderForm.controls["positions"]).push(new FormControl(this.positions[i], Validators.required));
      }
    }
  }


  eventLog: string = '';

  addPosition() {
    (<FormArray>this.orderForm.controls["positions"]).push(new FormControl("", Validators.required));
  }

  submit() {
    let val = this.orderForm.value;
    let order = {
      id: this.idValue,
      customer: {
        email: val.customerEmail,
        name: val.customerName,
        surname: val.customerSurname,
        phone: val.customerPhone
      },
      creationOrder: {
        positions: val.positions,
        type: val.type,
        provider: val.provider,
        dateCreateOrder: this.date,
        dateFulfillment: moment(val.completeDate).format('DD[.]MM[.]YYYY'),
        comment: val.comment
      }
    }
    if (this.url === 'addorder') {
      this.store.dispatch(new orderActions.AddOne(order));
    } else if (this.url === 'editorder') {
      this.store.dispatch(new orderActions.EditOrder(order));
    }
    
    this.resetForm();
  }

  resetForm() {
    let cont = this.orderForm.controls;
    cont.customerEmail.reset();
    cont.customerName.reset();
    cont.customerSurname.reset();
    cont.customerPhone.reset();
    cont.completeDate.reset();
    cont.comment.reset();
    cont.positions.reset();
  }

  generateId() {
    return `${this.type.charAt(0).toLowerCase()}-${this.moment.format('YYMMDD')}${this.ordersForLastMonth}`;
  }

  getNew() {
    return this.newOrder;
  }

  ngOnDestroy() {}

}
