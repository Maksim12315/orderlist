import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { MobxStore } from '../../mobxstore/mobxstore';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent {

  value;
  constructor(private router: Router,
    private store: MobxStore) { 
    this.value = store.counter;
  }

  start() {
    this.router.navigate(['/list']);
  }

  increment() {
    console.log('click', this.store.counter);
    this.store.increment();
  }

  decrement() {
    console.log('click', this.store.counter);
    this.store.decrement();
  }

}
