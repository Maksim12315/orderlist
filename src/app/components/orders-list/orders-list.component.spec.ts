import {
    TestBed
  } from '@angular/core/testing';
import { OrdersListComponent } from './orders-list.component';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { of } from 'rxjs';
import { ordersMock } from '../../services/orders.mock';
  

  
describe('OrderListComponent', () => {
  let comp: OrdersListComponent = null;
  let fixture; // test environment
  let storeSpy;
  let selectSpy;
  let nativeElement;
  let routerSpy;
  let navigateSpy;

  beforeEach(() => {
    storeSpy = jasmine.createSpyObj('Store', ['select', 'dispatch']);

    routerSpy = jasmine.createSpyObj('Router', ['navigate']);
  
    TestBed.configureTestingModule({
      declarations: [
        OrdersListComponent
      ],
      providers: [
        {provide: Router, useValue: routerSpy},
        {provide: Store, useValue: storeSpy}
      ]
    })

    fixture = TestBed.createComponent(OrdersListComponent);
    comp = fixture.componentInstance;
  });

  it(
    'should create the component',
    () => {
      expect(comp).toBeDefined();
    }
  )

  // expect, fail, xit, done = jasmine

  it(
    'should show first order data after component initialized', () => {
      selectSpy = storeSpy.select.and.returnValue(of(ordersMock));
      nativeElement = fixture.nativeElement; // access to DOM
      fixture.detectChanges(); // onInit
      compareValues(nativeElement.querySelector('#first-order-date-create').textContent, ordersMock[0].creationOrder.dateCreateOrder);
      compareValues(nativeElement.querySelector('#first-order-positions').textContent, ordersMock[0].creationOrder.positions.toString());
      compareValues(nativeElement.querySelector('#first-order-id').textContent, ordersMock[0].id);
      compareValues(nativeElement.querySelector('#first-order-type').textContent, ordersMock[0].creationOrder.type);
      compareValues(nativeElement.querySelector('#first-order-name-surname').textContent, `${ordersMock[0].customer.name} ${ordersMock[0].customer.surname}`);
      compareValues(nativeElement.querySelector('#first-order-provider').textContent, ordersMock[0].creationOrder.provider);
      compareValues(nativeElement.querySelector('#first-order-date-fulfillment').textContent, ordersMock[0].creationOrder.dateFulfillment);
    }
  )

  it(
    `should have as dayOnMilliseconds '24 * 60 * 60 * 1000'`,
    () => {
      expect(comp.dayOnMillisecond).toEqual(24 * 60 * 60 * 1000);
    }
  )

  it(
    `should have as dateNow today's date`,
    () => {
      expect(comp.dateNow).toEqual(moment().format('DD[.]MM[.]YYYY'));
    }
  )

  it(
    'statusMapper should return true (dateFulfillment more then date now)',
    () => {
      const nowDatePlusOneDay = moment(moment().valueOf() + comp.dayOnMillisecond).format('DD[.]MM[.]YYYY');
      expect(comp.statusMapper(nowDatePlusOneDay)).toEqual(true);
    }
  )

  it(
    'statusMapper should return false (dateFulfillment more then date now)',
    () => {
      const nowDateMinusOneDay = moment(moment().valueOf() - comp.dayOnMillisecond).format('DD[.]MM[.]YYYY');
      expect(comp.statusMapper(nowDateMinusOneDay)).toEqual(false);
    }
  )

  it(
    'canEdit should return true (dateFulfillment more then date now plus three days)',
    () => {
      const nowDatePlusFourDays = moment(moment().valueOf() + (comp.dayOnMillisecond * 4)).format('DD[.]MM[.]YYYY');
      expect(comp.canEdit(nowDatePlusFourDays)).toEqual(true);
    }
  )

  it(
    'canEdit should return false (dateFulfillment less then date now plus three days)',
    () => {
      const nowDatePlusTwoDays = moment(moment().valueOf() + (comp.dayOnMillisecond * 2)).format('DD[.]MM[.]YYYY');
      expect(comp.canEdit(nowDatePlusTwoDays)).toEqual(false);
    }
  )

  it(
    'should tell ROUTER to navigate when edit order clicked',
    () => {
      const firstOrder = ordersMock[0];
      comp.editOrder(firstOrder);

      const navigateSpy = routerSpy.navigate as jasmine.Spy;

      const firstNavigateArgs = navigateSpy.calls.first().args[0];
      const secondNavigateArgs = navigateSpy.calls.first().args[1].queryParams;

      const id = firstOrder.id;
      expect(`${firstNavigateArgs[0]}/${firstNavigateArgs[1]}`).toBe(`/editorder/${id}`);
      // expect( // compare called query params with first order values
      // Object.keys(navigateSpy.calls.first().args[1].queryParams)
      // .map(key => `${key}=${navigateSpy.calls.first().args[1].queryParams[key]}&`)
      // .join('')
      // .slice(0, -1)).toBe(
      // `customerEmail=${firstOrder.customer.email}&customerName=${firstOrder.customer.name}&customerSurname=${firstOrder.customer.surname}&customerPhone=${firstOrder.customer.phone}&positions=${firstOrder.creationOrder.positions.toString()}&type=${firstOrder.creationOrder.type}&provider=${firstOrder.creationOrder.provider}&completeDate=${firstOrder.creationOrder.dateFulfillment}&comment=${firstOrder.creationOrder.comment}&dateCreateOrder=${firstOrder.creationOrder.dateCreateOrder}`
      // );
      compareValues(secondNavigateArgs.customerEmail, firstOrder.customer.email);
      compareValues(secondNavigateArgs.customerName, firstOrder.customer.name);
      compareValues(secondNavigateArgs.customerSurname, firstOrder.customer.surname);
      compareValues(secondNavigateArgs.customerPhone, firstOrder.customer.phone);
      compareValues(secondNavigateArgs.positions, firstOrder.creationOrder.positions);
      compareValues(secondNavigateArgs.type, firstOrder.creationOrder.type);
      compareValues(secondNavigateArgs.provider, firstOrder.creationOrder.provider);
      compareValues(secondNavigateArgs.completeDate, firstOrder.creationOrder.dateFulfillment);
      compareValues(secondNavigateArgs.comment, firstOrder.creationOrder.comment);
      compareValues(secondNavigateArgs.dateCreateOrder, firstOrder.creationOrder.dateCreateOrder);
      
    }
  )

  function compareValues(expectedValue, receivedValue) {
    expect(expectedValue).toEqual(receivedValue);
  }
});