import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import * as moment from 'moment';
import { Observable } from 'rxjs';

import { Order } from '../../models/order';
import * as fromRoot from '../../store/reducers/index.reducers';
import * as orderActions from '../../store/actions/orders';
import { Router } from '@angular/router';

@Component({
  selector: 'app-orders-list',
  templateUrl: './orders-list.component.html',
  styleUrls: ['./orders-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OrdersListComponent implements OnInit {

  orders$ : Observable<Order[]>;
  dayOnMillisecond: number = 24 * 60 * 60 * 1000;
  dateNow: string = moment().format('DD[.]MM[.]YYYY');

  constructor(
    private store: Store<fromRoot.State>,
    private router: Router
    ) { }

  ngOnInit() {
    this.orders$ = this.store.select(fromRoot.getAllOrders);
    this.store.dispatch(new orderActions.LoadOrders());
  }

  statusMapper(dateFulfillment): boolean { // true = in process, false = done
    return (Number(moment(dateFulfillment, 'DD[.]MM[.]YYYY').format('x')) > Number(moment().format('x')));
  }

  editOrder(order) {
    this.router.navigate(
      ['/editorder', order.id],
      {
        queryParams: {
          "customerEmail": order.customer.email,
          "customerName": order.customer.name,
          "customerSurname": order.customer.surname,
          "customerPhone": order.customer.phone,
          "positions": order.creationOrder.positions,
          "type": order.creationOrder.type,
          "provider": order.creationOrder.provider,
          "completeDate": order.creationOrder.dateFulfillment,
          "comment": order.creationOrder.comment,
          "dateCreateOrder": order.creationOrder.dateCreateOrder
        }
      }
    );
  }

  canEdit(dateFulfillment): boolean {
    return (Number(moment(dateFulfillment, 'DD[.]MM[.]YYYY').format('x')) - this.dayOnMillisecond * 3 > Number(moment().format('x')));
  }

}
