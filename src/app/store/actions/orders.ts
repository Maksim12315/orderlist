import { Action } from '@ngrx/store';
import { Order } from '../../models/order';

export const ADD_ONE = '[Orders] Add One';
export const LOAD_ORDERS = '[Orders] Load';
export const LOAD_ORDERS_SUCCESS = '[Orders] Load Success';
export const ADD_ONE_SUCCESS = '[Orders] Add One Success';
export const GET_COUNT_ORDERS_MONTH = '[Orders] Get Count Orders';
export const GET_COUNT_ORDERS_LAST_MONTH_SUCCESS = '[Orders] Get Count Orders Success';
export const EDIT_ORDER = '[Orders] Edit Order';
export const EDIT_ORDER_SUCCESS = '[Orders] Edit Order Success';

export class AddOne implements Action {
    readonly type = ADD_ONE;
    constructor(public payload: Order) { }
}

export class LoadOrders implements Action {
    readonly type = LOAD_ORDERS;
    constructor() { }
}

export class EditOrder implements Action {
    readonly type = EDIT_ORDER;
    constructor(public payload: Order) {}
}
export class EditOrderSuccess implements Action {
    readonly type = EDIT_ORDER_SUCCESS;
    constructor(public payload: Order) {}
}

export class LoadOrdersSuccess implements Action {
    readonly type = LOAD_ORDERS_SUCCESS;
    constructor(public payload: Order[]){ };
}

export class AddOneSuccess implements Action {
    readonly type = ADD_ONE_SUCCESS;
    constructor(public payload: Order){ };
}

export class GetCountOrdersMonth implements Action {
    readonly type = GET_COUNT_ORDERS_MONTH;
    constructor() {}
}

export class GetCountOrdersLastMonthSuccess implements Action {
    readonly type = GET_COUNT_ORDERS_LAST_MONTH_SUCCESS;
    constructor(public payload: Number){ };
}



export type Action = AddOne | LoadOrders | LoadOrdersSuccess |
 AddOneSuccess | GetCountOrdersLastMonthSuccess | GetCountOrdersMonth |
 EditOrder | EditOrderSuccess;