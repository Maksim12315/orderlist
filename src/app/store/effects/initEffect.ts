import { Injectable } from '@angular/core';

import { Effect, Actions, ofType } from '@ngrx/effects';
import { OrdersService } from '../../services/orders.service'; 
import { map, switchMap, catchError } from 'rxjs/operators';

import * as ordersActions from '../actions/orders';

@Injectable()
export class OrderEffects {
  constructor(
    private actions$: Actions,
    private ordersService: OrdersService
  ) {}

  @Effect()
  loadOrders$ = this.actions$.pipe(
    ofType(ordersActions.LOAD_ORDERS),
    switchMap(() => {
      return this.ordersService
        .getOrders()
        .pipe(
          map(orders => new ordersActions.LoadOrdersSuccess(orders))
        );
    })
  );

  @Effect()
  addOrder$ = this.actions$.pipe(
    ofType(ordersActions.ADD_ONE),
    switchMap((action) => {
      return this.ordersService
        .postOrder(action)
        .pipe(
          map(order => new ordersActions.AddOneSuccess(order))
        )
    })
  );

  @Effect()
  editOrder$ = this.actions$.pipe(
    ofType(ordersActions.EDIT_ORDER),
    switchMap((action) => {
      return this.ordersService
        .putOrder(action)
        .pipe(
          map(order => new ordersActions.EditOrderSuccess(order))
        )
    })
  );
  // @Effect()
  // getOrdersCountFromLastMonth$ = this.actions$.pipe(
  //   ofType(ordersActions.GET_COUNT_ORDERS_MONTH),
  //   switchMap(() => {
  //     return this.ordersService
  //       .getOrdersCount()
  //       .pipe(
  //         map(ordersCount => {
  //           console.log(ordersCount);
  //           new ordersActions.GetCountOrdersLastMonthSuccess(ordersCount)
  //         }
  //       )
  //   )}
  // ));
}