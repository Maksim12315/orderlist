import * as orderAction from '../actions/orders';
import { Customer } from 'src/app/models/customer';

export interface CurrentUser {
    currentUser: Customer;
}

export const UserState: CurrentUser = {
    currentUser: {
        email: '',
        name: '',
        surname: '',
        phone: ''
    }
}

export function reducer(state = UserState, 
    action: orderAction.Action) {
        switch (action.type) {
            default:
                return state;
        }
}