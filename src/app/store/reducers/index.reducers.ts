import { ActionReducerMap, createSelector, createFeatureSelector, } from '@ngrx/store';
import * as fromOrders from './orders';
import * as moment from 'moment';

export interface State {
    orders: fromOrders.State;
}

export const reducers: ActionReducerMap<State> = {
    orders: fromOrders.reducer
};
export const getOrderState = 
    createFeatureSelector<fromOrders.State>('orders');


export const getOrders = createSelector(
    getOrderState,
    fromOrders.getOrders,
);
export const getAllOrders = createSelector(
    getOrders,
    (orders) => {
        return orders;
    }
);

// export const getOrder = createSelector(
//     getOrdersIds,
//     (orders) => {

//     } 
// )

export const getOrdersForTheLastMongth = createSelector(
    getOrders,
    (orders) => {
        let month = moment().format('MM[.]YYYY');
        let count = 1;
        for(let i = 0; i < orders.length; i++) {
            if(orders[i].creationOrder.dateCreateOrder.slice(3) === month) {
                count++
            }
        }
        return count;
    }
); 


