import * as orderAction from '../actions/orders';
import { Order } from '../../models/order';

export interface State {
  orders: Order[]
}

export const initialState: State = {
  orders: [
    {
        id: "o-15100416",
        customer: {
            email: "true@gmail.com",
            name: "John",
            surname: "Smith",
            phone: "+380961918373"
        },
        creationOrder: {
            positions: ["pen"],
            type: "o",
            provider: "Pen@pen",
            dateCreateOrder: "23.12.2015",
            dateFulfillment: "26.12.2015",
            comment: "Ty for pens and pents"
        }
    }
  ]
};

export function reducer(state = initialState, 
  action: orderAction.Action) {
    switch (action.type) {
      case orderAction.ADD_ONE: {
        const newOrder: Order = action.payload;
        return {
          orders: [...state.orders, newOrder]
        };
      }
      case orderAction.LOAD_ORDERS: {
        return {
          ...state
        }
      }
      case orderAction.LOAD_ORDERS_SUCCESS: {
        const newState: Order[] = action.payload;
        return {
          orders: newState
        }
      }
      case orderAction.ADD_ONE_SUCCESS: {
        const newOrder: Order = action.payload;
        return {
          ...state,
          orders: [...state.orders, newOrder]
        }; 
      }
      case orderAction.GET_COUNT_ORDERS_MONTH: {
        return state;
      }
      case orderAction.GET_COUNT_ORDERS_LAST_MONTH_SUCCESS: {
        const numb = action.payload;
        return {
          orders:[...state.orders],
          countOrders: numb
        }
      }

      case orderAction.EDIT_ORDER: {
        return state
      }
      case orderAction.EDIT_ORDER_SUCCESS: {
        for (let i = 0; i < state.orders.length; i++) {
          if (state.orders[i].id === action.payload.id) {
            state.orders[i] = action.payload;
          }
        }
        return state;
      }
      
      default:
        return state;
      }
}

export const getOrders = (state: State) => state.orders;
export const getOrderById = (state: State, id: string) => {
  for(let i = 0; i < state.orders.length; i++) {
    if(state.orders[i].id === id) {
      return state.orders[i];
    }
  }
};
