import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { StoreModule } from '@ngrx/store';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { OrdersListComponent } from './components/orders-list/orders-list.component';
import { AddOrderComponent } from './components/add-order/add-order.component';
import { reducers } from './store/reducers/index.reducers';
import { OrderEffects } from '../app/store/effects/initEffect';
import { OrdersService } from './services/orders.service';
import { EntryComponent } from './components/entry/entry.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { MyAccComponent } from './components/my-acc/my-acc.component';

import { MobxAngularModule } from 'mobx-angular';
import { MobxStore } from './mobxstore/mobxstore';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    OrdersListComponent,
    AddOrderComponent,
    EntryComponent,
    RegistrationComponent,
    MyAccComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    EffectsModule.forRoot([OrderEffects]),
    StoreModule.forRoot(reducers),
    BsDatepickerModule.forRoot(),
    MobxAngularModule
  ],
  providers: [OrdersService, MobxStore],
  bootstrap: [AppComponent]
})
export class AppModule { }
