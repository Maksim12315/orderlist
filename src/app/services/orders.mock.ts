export const ordersMock = [{
  "id": "о-1906174",
  "customer": {
    "email": "asd@ss",
    "name": "as",
    "surname": "as",
    "phone": "12"
  },
  "creationOrder": {
    "positions": [
      "123"
    ],
    "type": "Опт",
    "provider": "Undead",
    "dateCreateOrder": "17.06.2019",
    "dateFulfillment": "29.06.2019",
    "comment": "123"
  }
}];

export const errorResponseMock = {
  "message": "test 404 error",
  "status": 404, 
  "statusText": "Not Found"
}

// Faker.js