import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { Order } from '../models/order';
import { Action } from '../store/actions/orders';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  private url = environment.production.apiurl;
  constructor(private httpClient: HttpClient) {}
  
  getOrders(): Observable<Order[]> {
    return this.httpClient.get<Order[]>(this.url);
  }

  postOrder(action: Action): Observable<Order> {
    return this.httpClient.post<Order>(this.url, action['payload']);
  }

  putOrder(action: Action): Observable<Order>{
    return this.httpClient.put<Order>(this.url +'/' +action['payload'].id, action['payload']);
  }

}
