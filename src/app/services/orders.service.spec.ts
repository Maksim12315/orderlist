import {
  TestBed,
  inject,
  fakeAsync,
  tick
} from '@angular/core/testing';

import { OrdersService } from './orders.service';

import { ordersMock, errorResponseMock } from './orders.mock';
import { of, throwError } from 'rxjs';
import * as orderActions from '../../app/store/actions/orders';

describe('OrdersService', () => {

  let httpClientSpy: {
    get: jasmine.Spy,
    post: jasmine.Spy,
    put: jasmine.Spy
  };
  let orderService: OrdersService;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put']);
    orderService = new OrdersService(<any> httpClientSpy);
  });

  testServiceMethod('getOrders', 'get', ordersMock, errorResponseMock, 'should return expected orders');
  testServiceMethod('postOrder', 'post', ordersMock[0], errorResponseMock, 'should successfully add order', new orderActions.AddOne(ordersMock[0]));
  testServiceMethod('putOrder', 'put', ordersMock[0], errorResponseMock, 'should successfully edit order', new orderActions.EditOrder(ordersMock[0]))

  function testServiceMethod(serviceMethodName, spyMethodName, expectedSuccessValue, expectedErrorValue, testDescription, payload?) {
    it(`${testDescription} (HttpClient called once)`, () => {
      httpClientSpy[spyMethodName].and.returnValue(of(expectedSuccessValue));
  
      orderService[serviceMethodName](payload).subscribe(
        orders => expect(orders).toEqual(expectedSuccessValue, 'expected orders'),
        fail
      )
  
      expect(httpClientSpy[spyMethodName].calls.count()).toBe(1, 'one call');
    })
  
    it('should return an error when the server returns a 404', () => {
      httpClientSpy[spyMethodName].and.returnValue(throwError(expectedErrorValue));
  
      orderService[serviceMethodName](payload).subscribe(
        () => fail('expected an error'),
        err => expect(err.message).toBe(expectedErrorValue.message)
      )
    })
  }
});
