import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { Customer } from '../models/customer';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private subj: BehaviorSubject<Customer> = new BehaviorSubject(null);
  constructor() { }

  sendData(data) {
    this.subj.next(data)
  }

  getData() {
    return this.subj;
  }
}
