import { observable, action } from 'mobx-angular';
import { Injectable } from '@angular/core';

@Injectable()
export class MobxStore {
  @observable counter : number = 0;
  @action increment() {
      this.counter = this.counter + 1;
  }
  @action decrement() {
      this.counter = this.counter - 1;
  }
}