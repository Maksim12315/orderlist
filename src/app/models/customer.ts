export interface Customer {
    email: string;
    name: string;
    surname: string;
    phone: string;
}