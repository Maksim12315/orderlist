import { Customer } from './customer';
import { CreationOrder } from './creationOrder';

export interface Order {
    id: string;
    customer: Customer;
    creationOrder: CreationOrder;
}