export interface CreationOrder {
    positions: string[];
    type: string;
    provider: string;
    dateCreateOrder: string;
    dateFulfillment: string;
    comment?: string;
}