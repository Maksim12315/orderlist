import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { OrdersListComponent } from './components/orders-list/orders-list.component';
import { AddOrderComponent } from './components/add-order/add-order.component';
import { MyAccComponent } from './components/my-acc/my-acc.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'list', component: OrdersListComponent},
  { path: 'addorder', component: AddOrderComponent},
  { path: 'editorder/:id', component: AddOrderComponent},
  { path: 'account', component: MyAccComponent},
  { path: '**', redirectTo: '/' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
